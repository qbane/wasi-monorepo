import Stats from "./Stats";
import Dirent from "./Dirent";
import {
  Volume as _Volume,
  StatWatcher,
  FSWatcher,
  toUnixTimestamp,
  IReadStream,
  IWriteStream,
  DirectoryJSON,
} from "./volume";
import { IPromisesAPI } from "./promises";
const { fsSyncMethods, fsAsyncMethods } = require("fs-monkey/lib/util/lists");
import { constants } from "./constants";
const { F_OK, R_OK, W_OK, X_OK } = constants;

export type { DirectoryJSON };
export const Volume = _Volume;

// Default volume.
export const vol = new _Volume();

export interface IFs extends _Volume {
  constants: typeof constants;
  Stats: new (...args) => Stats;
  Dirent: new (...args) => Dirent;
  StatWatcher: new () => StatWatcher;
  FSWatcher: new () => FSWatcher;
  ReadStream: new (...args) => IReadStream;
  WriteStream: new (...args) => IWriteStream;
  promises: IPromisesAPI;
  _toUnixTimestamp;
}

export function createFsFromVolume(vol: _Volume): IFs {
  const fs = { F_OK, R_OK, W_OK, X_OK, constants, Stats, Dirent } as any as IFs;

  // Bind FS methods.
  for (const method of fsSyncMethods)
    if (typeof vol[method] === "function") fs[method] = vol[method].bind(vol);
  for (const method of fsAsyncMethods)
    if (typeof vol[method] === "function") fs[method] = vol[method].bind(vol);

  fs.StatWatcher = vol.StatWatcher;
  fs.FSWatcher = vol.FSWatcher;
  fs.WriteStream = vol.WriteStream;
  fs.ReadStream = vol.ReadStream;
  fs.promises = vol.promises;

  fs._toUnixTimestamp = toUnixTimestamp;

  return fs;
}

export const fs: IFs = createFsFromVolume(vol);

// the folloing lines are to replicate the CommonJS-style export
// declare let module;
// module.exports = { ...module.exports, ...fs };
export { F_OK, R_OK, W_OK, X_OK, constants, Stats, Dirent };
const fsStatWatcher = vol.StatWatcher;
const fsFSWatcher = vol.FSWatcher;
const fsWriteStream = vol.WriteStream;
const fsReadStream = vol.ReadStream;
const fspromises = vol.promises;
export const renameSync = fs.renameSync;
export const ftruncateSync = fs.ftruncateSync;
export const truncateSync = fs.truncateSync;
export const chownSync = fs.chownSync;
export const fchownSync = fs.fchownSync;
export const lchownSync = fs.lchownSync;
export const chmodSync = fs.chmodSync;
export const fchmodSync = fs.fchmodSync;
export const lchmodSync = fs.lchmodSync;
export const statSync = fs.statSync;
export const lstatSync = fs.lstatSync;
export const fstatSync = fs.fstatSync;
export const linkSync = fs.linkSync;
export const symlinkSync = fs.symlinkSync;
export const readlinkSync = fs.readlinkSync;
export const realpathSync = fs.realpathSync;
export const unlinkSync = fs.unlinkSync;
export const rmdirSync = fs.rmdirSync;
export const mkdirSync = fs.mkdirSync;
export const mkdirpSync = fs.mkdirpSync;
export const readdirSync = fs.readdirSync;
export const closeSync = fs.closeSync;
export const openSync = fs.openSync;
export const utimesSync = fs.utimesSync;
export const futimesSync = fs.futimesSync;
export const fsyncSync = fs.fsyncSync;
export const writeSync = fs.writeSync;
export const readSync = fs.readSync;
export const readFileSync = fs.readFileSync;
export const writeFileSync = fs.writeFileSync;
export const appendFileSync = fs.appendFileSync;
export const existsSync = fs.existsSync;
export const accessSync = fs.accessSync;
export const fdatasyncSync = fs.fdatasyncSync;
export const mkdtempSync = fs.mkdtempSync;
export const copyFileSync = fs.copyFileSync;
export const createReadStream = fs.createReadStream;
export const createWriteStream = fs.createWriteStream;
export const rename = fs.rename;
export const ftruncate = fs.ftruncate;
export const truncate = fs.truncate;
export const chown = fs.chown;
export const fchown = fs.fchown;
export const lchown = fs.lchown;
export const chmod = fs.chmod;
export const fchmod = fs.fchmod;
export const lchmod = fs.lchmod;
export const stat = fs.stat;
export const lstat = fs.lstat;
export const fstat = fs.fstat;
export const link = fs.link;
export const symlink = fs.symlink;
export const readlink = fs.readlink;
export const realpath = fs.realpath;
export const unlink = fs.unlink;
export const rmdir = fs.rmdir;
export const mkdir = fs.mkdir;
export const mkdirp = fs.mkdirp;
export const readdir = fs.readdir;
export const close = fs.close;
export const open = fs.open;
export const utimes = fs.utimes;
export const futimes = fs.futimes;
export const fsync = fs.fsync;
export const write = fs.write;
export const read = fs.read;
export const readFile = fs.readFile;
export const writeFile = fs.writeFile;
export const appendFile = fs.appendFile;
export const exists = fs.exists;
export const access = fs.access;
export const fdatasync = fs.fdatasync;
export const mkdtemp = fs.mkdtemp;
export const copyFile = fs.copyFile;
export const watchFile = fs.watchFile;
export const unwatchFile = fs.unwatchFile;
export const watch = fs.watch;
export {
  fsStatWatcher as StatWatcher,
  fsFSWatcher as FSWatcher,
  fsWriteStream as WriteStream,
  fsReadStream as ReadStream,
  fspromises as promises,
  toUnixTimestamp as _toUnixTimestamp,
  vol as __vol,
};
export const semantic = true;
