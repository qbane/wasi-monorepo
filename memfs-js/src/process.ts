type nextTickFunc = (callback: (...args) => void, ...args) => void;

// Here we mock the global `process` variable in case we are not in Node's environment.

export interface IProcess {
  getuid?(): number;

  getgid?(): number;

  cwd(): string;

  platform: string;
  nextTick: nextTickFunc;
  emitWarning: (message: string, type: string) => void;
  env: {};
}

let _setImmediate: nextTickFunc | undefined = undefined;

/**
 * Looks to return a `process` object, if one is available.
 *
 * The global `process` is returned if defined;
 * otherwise `require('process')` is attempted.
 *
 * If that fails, `undefined` is returned.
 *
 * @return {IProcess | undefined}
 */
const maybeReturnProcess = (): IProcess | undefined => {
  if (typeof process !== "undefined") {
    return process;
  }

  if (typeof require !== "undefined") {
    try {
      return require("process");
    } catch {}
  }
  return undefined;
};

export function createProcess(): IProcess {
  const p: IProcess = maybeReturnProcess() || ({} as IProcess);

  if (!p.cwd) p.cwd = () => "/";
  // FIXME: to be removed as per
  //        https://github.com/streamich/memfs/issues/934
  if (!p.nextTick) p.nextTick = require("./setImmediate").default;
  if (!p.emitWarning)
    p.emitWarning = (message, type) => {
      // tslint:disable-next-line:no-console
      console.warn(`${type}${type ? ": " : ""}${message}`);
    };
  if (!p.env) p.env = {};
  return p;
}

export default createProcess();
