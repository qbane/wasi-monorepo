import path from 'node:path'
import { defineConfig } from 'rollup'
import commonjs from '@rollup/plugin-commonjs'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import replace from '@rollup/plugin-replace'
import alias from '@rollup/plugin-alias'
import inject from '@rollup/plugin-inject'

/** @type {import('rollup').RollupOptions} */
export default defineConfig({
  input: {
    'wasi-js/index': 'wasi-js/dist/index.js',
    'wasi-js/bindings/browser': 'wasi-js/dist/bindings/browser.js',
    'memfs': 'memfs-js/lib/index.js',
    'unionfs': 'unionfs/lib/index.js',
  },
  output: {
    dir: 'dist',
    sourcemap: true,

    // for Vite
    // dir: 'esm_node_modules_',
    // preserveModules: true,
  },
  plugins: [
    replace({
      preventAssignment: true,
      values: {
        'process.env.NODE_DEBUG': 'false',
      },
    }),
    nodeResolve({
      browser: true,
      preferBuiltins: false,
    }),
    commonjs({
      transformMixedEsModules: true,
    }),
    alias({
      entries: [
        { find: 'path', replacement: 'path-browserify' },
        { find: 'stream', replacement: 'stream-browserify' },
        // { find: 'buffer', replacement: path.resolve('interop.js') },
        // { find: 'assert', replacement: 'assert/build/assert.js' },
        // { find: 'util', replacement: 'util/util.js' },
        // { find: 'events', replacement: 'events/events.js' },
      ],
    }),
    inject({
      modules: {
        Buffer: [path.resolve('interop.js'), 'Buffer'],
      },
    }),
  ],
})
